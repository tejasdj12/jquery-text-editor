/**
 * JQuery Text Editor
 *
 * A simple JQuery based WYSIWYG text editor.
 *
 * @author Tejas Jadhav <tejasdj12@gmail.com>
 * @copyright Copyright (c) 2014, Tejas Jadhav
 */

(function ($) {
	$.fn.textEditor = function (options) {
		// Default settings.
		var settings = $.extend({
			debug: false
		}, options);

		// Assign the parent element for further reuse.
		var parent = this;

		// Save the current HTML content.
		var htmlContent = parent.html();

		// Remove all the children from the parent.
		parent.empty();

		// Add the text-editor class. Required for CSS.
		parent.addClass('text-editor');

		// Create the editorArea element and make it editable.
		var editorArea = $(document.createElement('div'))
			.addClass('editor-area')
			.attr('contenteditable', 'true');

		// Create the toolbar element
		var toolbar = $(document.createElement('div'))
			.addClass('toolbar');

		// Attach the elements to the parent.
		parent.append(editorArea);
		parent.prepend(toolbar);

		// Dump the HTML content in the editorArea.
		editorArea.html(htmlContent);

		// ---------------------------------------------- Controls ------------------------------------------------ //
		// A toolbox array containing all the tool objects.
		var toolbox = [
			{name: 'block', title: null, command: 'formatBlock', tooltip: 'Format', attr: {
				type   : 'select',    // It will be a select input.
				// And below are the options for that.
				options: [
					{value: 'h1', title: 'Heading 1'},
					{value: 'h2', title: 'Heading 2'},
					{value: 'h3', title: 'Heading 3'},
					{value: 'h4', title: 'Heading 4'},
					{value: 'h5', title: 'Heading 5'},
					{value: 'h6', title: 'Heading 6'},
					{value: 'p', title: 'Paragraph', selected: true},
					{value: 'blockquote', title: 'Blockquote'},
					{value: 'address', title: 'Address'}
				]
			}
			},
			{name: 'font-family', title: null, command: 'fontName', tooltip: 'Fonts', attr: {
				type   : 'select',
				options: [
					{value: 'Arial', title: '<span style = "font-family: Arial, sans-serif;">Arial</span>', selected: true},
					{value: 'Courier New', title: '<span style = "font-family: Courier New, monospace;">Courier New</span>'},
					{value: 'Georgia', title: '<span style = "font-family: Georgia, serif;">Georgia</span>'},
					{value: 'Times New Roman', title: '<span style = "font-family: Times New Roman, serif;">Times New Roman</span>'},
					{value: 'Trebuchet', title: '<span style = "font-family: Trebuchet, sans-serif;">Trebuchet</span>'},
					{value: 'Verdana', title: '<span style = "font-family: Verdana, sans-serif;">Verdana</span>'}
				]
			}
			},
			{name: 'bold', title: '<strong>B</strong>', command: 'bold', tooltip: 'Bold', attr: null},
			{name: 'italic', title: '<i>I</i>', command: 'italic', tooltip: 'Italic', attr: null},
			{name: 'underline', title: '<u>U</u>', command: 'underline', tooltip: 'Underline', attr: null},
			{name: 'left', title: 'Left', command: 'justifyLeft', tooltip: 'Left Alignment', attr: null},
			{name: 'center', title: 'Center', command: 'justifyCenter', tooltip: 'Center Alignment', attr: null},
			{name: 'right', title: 'Right', command: 'justifyRight', tooltip: 'Right Alignment', attr: null},
			{name: 'justify', title: 'Justify', command: 'justifyFull', tooltip: 'Justify', attr: null},
			{name: 'ul', title: 'UL', command: 'insertUnorderedList', tooltip: 'Unordered List', attr: null},
			{name: 'ol', title: 'OL', command: 'insertOrderedList', tooltip: 'Ordered List', attr: null},
			{name: 'remove-formatting', title: 'T&cross;', command: 'removeFormat', tooltip: 'Remove Formatting', attr: null}
		];
		// ------------------------------------------- Controls end ----------------------------------------------- //

		// Add all the tools to the toolbar.
		// @todo DOM Manipulation in a loop is too bad from performance point of view. Need to find a workaround.
		toolbox.forEach(function (tool) {
			if (tool.attr != null) {
				if (tool.attr.type == 'select') {
					var selectMenu = $(document.createElement('select'))
						.addClass('tool')
						.attr('data-command', tool.command)
						.attr('data-type', 'select')
						.attr('title', tool.tooltip);

					tool.attr.options.forEach(function (value) {
						var option = $(document.createElement('option')).val(value.value);
						if (value.title != null) option.html(value.title);
						else option.html(value.value);
						if (value.selected) option.attr('selected', 'true');
						selectMenu.append(option);
					});

					toolbar.append(selectMenu);
				}
			} else {
				toolbar.append($(document.createElement('button'))
					.addClass('tool')
					// Don't know why JQuery.data() won't work here. Using .attr() as workaround.
					.attr('data-command', tool.command)
					.attr('data-type', 'button')
					.attr('title', tool.tooltip)
					.html(tool.title));
			}
		});

		// Add an onclick listener to all the tools.
		toolbar.on('click', '.tool', function () {
			// @todo Make execCommand restricted to the editorArea only, not the whole document.
			if ($(this).data('type') == 'button') document.execCommand($(this).data('command'));
		});

		// Add an onlick listener to all the options.
		toolbar.on('click', 'option', function () {
			if ($(this).parent().data('type') == 'select')
				document.execCommand($(this).parent().data('command'), false, $(this).parent().val());
		});

		// Add an onchange listener to all the select inputs.
		toolbar.on('change', 'select', function () {
			if ($(this).data('type') == 'select') document.execCommand($(this).data('command'), false, $(this).val());
		});

	};
}(jQuery));